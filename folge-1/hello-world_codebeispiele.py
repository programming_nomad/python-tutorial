# damit diese Datei auch mir Python einfach aufgerufen werden kann,
# habe ich alle Eingaben jeweils in eine print-Funktion gepackt

# hello world
print("hello world")

# rechnen
print(3+3)
print(4*4)
print(5-3)
print(7/3)

# damit Ihr den Fehler bekommt, müsst Ihr bei
# der nächsten Zeile das Raute-Zeichen entfernen
# print(7/0)

# variablen
x=10
print(x)

print(5)
print(4)
print(3)
print(2)
print(1)

print(x)
print(x*x)

y=x*x
print(y)
print(x)

x=x*x
print(x)

x=x*x
print(x)

# if und else verzweigungen (wenn ..., dann ...)
if x>5:
    print('ja')

if x>5:
    print('ja')
else:
    print('nein')

x=1
if x>5:
    print('ja')
else:
    print('nein')

# die for-Schleife - hier müsst ihr ebenfalls die zwei Kommentare
# weg machen. Sonst wäre alles voll mit Zahlen.
#for i in range(5000):
#    print(i)
