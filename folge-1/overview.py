


print('\n\n-------------------------------------')
print('+ Programmieren lernen mit Python 3 +')
print('-------------------------------------\n')

print('Folge: 1\n')

print('Video-Inhalt')
print('............')
print('-> hello world')
print('-> Code-Beispiele')
print('-> Vorteile von Python')
print('-> Installationshinweise\n')

print('Links')
print('............')
print('-> https://www.python.org')
print('-> https://www.python.org/shell/')
print('-> https://www.python.org/downloads/\n')
